import ClassicEditorBase from '@ckeditor/ckeditor5-editor-classic/src/classiceditor';
import Essentials from '@ckeditor/ckeditor5-essentials/src/essentials';
import Autoformat from '@ckeditor/ckeditor5-autoformat/src/autoformat';
import Bold from '@ckeditor/ckeditor5-basic-styles/src/bold';
import Italic from '@ckeditor/ckeditor5-basic-styles/src/italic';
import BlockQuote from '@ckeditor/ckeditor5-block-quote/src/blockquote';
import Heading from '@ckeditor/ckeditor5-heading/src/heading';
import Link from '@ckeditor/ckeditor5-link/src/link';
import List from '@ckeditor/ckeditor5-list/src/list';
import CodeBlock from '@ckeditor/ckeditor5-code-block/src/codeblock';
import Strikethrough from '@ckeditor/ckeditor5-basic-styles/src/strikethrough';
import Underline from '@ckeditor/ckeditor5-basic-styles/src/underline';
import TextTransformation from '@ckeditor/ckeditor5-typing/src/texttransformation';
import Markdown from '@ckeditor/ckeditor5-markdown-gfm/src/markdown';
import SourceEditing from '@ckeditor/ckeditor5-source-editing/src/sourceediting';

export default class ClassicEditor extends ClassicEditorBase {}

// Plugins to include in the build.
ClassicEditor.builtinPlugins = [
	Essentials,
	Markdown,
	SourceEditing,
	Autoformat,
	Bold,
	Italic,
	BlockQuote,
	Heading,
	Link,
	List,
	Strikethrough,
	Underline,
	CodeBlock,
	TextTransformation
];

// Editor configuration.
ClassicEditor.defaultConfig = {
	toolbar: {
		items: [
			'heading',
			'|',
			'bold',
			'italic',
			'strikethrough',
			'underline',
			'|',
			'link',
			'numberedList',
			'bulletedList',
			'|',
			'codeBlock',
			'blockQuote',
			'|',
			'undo',
			'redo',
			'|',
			'sourceEditing'
		]
	},
	codeBlock: {
		languages: [
			{ language: 'plaintext', label: 'Plain text', class: 'ck-plain' },
			{ language: 'javascript', label: 'JavaScript', class: 'ck-javascript-code' },
			{ language: 'JSON', label: 'JSON', class: 'ck-json' },
			{ language: 'html', label: 'HTML', class: 'ck-html' },
			{ language: 'css', label: 'CSS', class: 'ck-css' }
		]
	},
	// This value must be kept in sync with the language defined in webpack.config.js.
	language: 'en'
};
